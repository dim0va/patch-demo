package com.dimova.patch.demo.controller;

import com.dimova.patch.demo.entity.Customer;
import com.dimova.patch.demo.exception.NotFoundException;
import com.dimova.patch.demo.service.impl.CustomerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@Validated
@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private CustomerServiceImpl customerService;
    private ObjectMapper objectMapper;

    public CustomerController (CustomerServiceImpl customerService, ObjectMapper objectMapper
                               ) {
        this.customerService = customerService;
        this.objectMapper = objectMapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getOneById(@Valid @PathVariable Long id) {
        Customer customer = customerService.getById(id).orElseThrow(NotFoundException::new);
        return ResponseEntity.ok(customer);
    }

    @PatchMapping(path = "/{id}", consumes = {"application/merge-patch+json"})
    public ResponseEntity<Customer> patchCustomer(@PathVariable Long id, @RequestBody Map<String, String> customerData)
            throws InvocationTargetException, IllegalAccessException {

        Customer customer = customerService.getById(id).orElseThrow(NotFoundException::new);

        if(customer!=null && customerData!=null) {
            //ObjectMapper Version:
            Customer toBePatchedCustomer = objectMapper.convertValue(customerData, Customer.class);
            Customer patchedCustomer = customerService.patch(toBePatchedCustomer, id);
            return ResponseEntity.ok(patchedCustomer);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
