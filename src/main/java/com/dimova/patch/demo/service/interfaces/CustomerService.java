package com.dimova.patch.demo.service.interfaces;

import com.dimova.patch.demo.entity.Customer;
import java.util.List;
import java.util.Optional;

public interface CustomerService{
    Optional<Customer> getById(long id);

    List<Customer> getAll ();

    Customer create(Customer entity);

    Customer update(Customer entity, long id);

    void delete(long id);
}
