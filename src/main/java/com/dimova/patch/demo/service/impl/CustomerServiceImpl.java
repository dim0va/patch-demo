package com.dimova.patch.demo.service.impl;

import com.dimova.patch.demo.service.interfaces.CustomerService;
import com.dimova.patch.demo.entity.Customer;
import com.dimova.patch.demo.repository.CustomerRepo;
import com.dimova.patch.demo.config.NullAwareBeanUtilsBean;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepo customerRepo;
    private NullAwareBeanUtilsBean beanUtils;

    public CustomerServiceImpl (CustomerRepo customerRepo, NullAwareBeanUtilsBean beanUtils) {
        this.customerRepo = customerRepo;
        this.beanUtils = beanUtils;
    }

    @Override
    public Optional<Customer> getById(long id) {
        return customerRepo.findById(id);
    }

    @Override
    public List<Customer> getAll() {
        return customerRepo.findAll();
    }

    @Override
    public Customer create(Customer entity) {
        return customerRepo.save(entity);
    }

    @Override
    public Customer update(Customer entity, long id) {
        entity.setId(id);
        return customerRepo.save(entity);
    }

    @Override
    public void delete(long id) {
        customerRepo.deleteById(id);
    }

    public Customer patch(Customer toBePatched, long id) throws InvocationTargetException, IllegalAccessException {
        Optional<Customer> optionalCustomer = customerRepo.findById(id);

        if (optionalCustomer.isPresent()) {
            Customer customer = optionalCustomer.get();
            // bean utils will copy non null values from toBePatched to customer.
            beanUtils.copyProperties(customer, toBePatched);
            return update(customer, id);
        }
        return null;
    }
}
