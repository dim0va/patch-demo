package com.dimova.patch.demo.entity;

import lombok.*;
import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "customers")
public class Customer {
    @Id
    @Column(name = "customer_id")
    private long id;
    private String firstName;
    private String lastName;
    private String telephone;
}
