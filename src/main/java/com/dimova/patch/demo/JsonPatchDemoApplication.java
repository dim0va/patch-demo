package com.dimova.patch.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonPatchDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsonPatchDemoApplication.class, args);
    }

}
